# Proyecto final-Prog.Grafica

Se tienen las siguientes interfaces graficas(UI):

● Una pantalla de inicio de sesión (usuario y contraseña). 
● Formulario de proyecto debe tener las siguientes propiedades como mínimo: nombre, fecha de lanzamiento, descripción, categoría, el tipo licenciamiento, link al sitio web o repositorio del proyecto y la imagen. 
● Tabla de visualización de los diferentes proyectos regidtrados.

El proyecto puede ser desarrollado como web o aplicación mobile (onsen). Queda a criterio de cada grupo:

● El proyecto se desarrolló como web. 

Para las interfaces graficas del proyecto usamos:

● Las bibliotecas de jQuery
● Nos apoyamos en HTML puro para el texto como tal, CSS para el diseño y JavaScript para la funcionalidad.

Proyecto final de programacion grafica UIP.